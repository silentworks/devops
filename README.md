# Vagrant setup for Apache, PHP, MySQL, xdebug

You can install this repository by running the following commands:

```bash
git clone git@bitbucket.org:silentworks/devops.git
cd devops
vagrant up
```